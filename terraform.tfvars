region = <region inside of quotes, ex. "us-west"> 

zone = <same format as region above with the added zone tacked onto the end, ex. "us-west1-c">

gcp_project = <name of project inside of quotes, ex. "kube-build-josh">

credentials = <file path location for gcloud json credentials>

