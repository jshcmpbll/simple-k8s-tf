resource "google_container_cluster" "primary" {
  name     = "gitlab-cluster"
  initial_node_count = 3

  pod_security_policy_config = {
    enabled = false
  }

}


